# each time we look at 5 + 7 + 5 weights, and then see if we find a haiku

import fileinput
class Input_Reader:
    def __init__(self):
        self._input_list = []
        counter = 0
        for line in fileinput.input():
            if (counter > 0):
                self._input_list.append(list(map(int, (line.split()))))
            counter += 1;
        for i in range(0, len(self._input_list)):
            self._input_list[i].insert(0,0)

    def get_input(self):
        return self._input_list;

# get all the sullables within 17 weight, if cannot find exactly 17 return 
# an empty list
def in_range_syllables(start_index, sentence):
    sum = 0
    cur_index = start_index + 1
    in_range_list = []
    while (sum <= 17 and cur_index < len(sentence) - 1):
        in_range_list.append(sentence[cur_index])
        sum += sentence[cur_index]
        cur_index += 1
        if (sum > 17):
            return []
        if (sum == 17 and sentence[cur_index] == 0 and sentence[start_index] == 0):
            return in_range_list


# find if it is partial haiku
def is_partial_haiku(partial_line):
    sum  = 0
    flag_1, flag_2, flag_3 = False, False, False
    for n in partial_line:
        sum += n
        if (sum == 5):
            flag_1 = True
            continue
        if (sum == 12):
            flag_2 = True
            continue
        if (sum == 17):
            flag_3 = True
            continue

    return (flag_1 and flag_2 and flag_3)


def is_haiku(line):
    if (len(line) == 0 or sum(line) < 17):
        print("No Haiku")
        return

    sentence_counter = 0
    for i in range(0, len(line)):
        if (line[i] == 0):
            sentence_counter += 1
            in_range = in_range_syllables(i, line)
            if (in_range != None):
                flag = is_partial_haiku(in_range_syllables(i, line))
                if (flag):
                    print(sentence_counter)
                    return
    print("No Haiku")
    return;

def print_result(input):
    for line in input:
        is_haiku(line)

if __name__ == '__main__':
    input_reader = Input_Reader()
    input_list   = input_reader.get_input()
    print_result(input_list)