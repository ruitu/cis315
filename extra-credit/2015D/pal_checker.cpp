#include <iostream>
#include <vector>
using namespace std;
/*
	imagin there is a string A
	i  		     j
	--------------

	string B

	m 			k
	-------------
	
	then we check if every time we can find a match between 
	| i - j  |  m - k  |  i - k  |  m - j  |
	if we find a match we advance the index until we run out of index

*/

class PalindromChecker{
public:
	PalindromChecker(std::string a, std::string b);
	PalindromChecker();
	virtual ~PalindromChecker(){};
	int findPalindrom();
	void printPal();
	int checkPalHelper(int i, int j, int k, int m);
	int checkPal();

private:
	std::string first_str;
	std::string second_str;
};


PalindromChecker::PalindromChecker (std::string aStr, std::string bStr){
	this->first_str = aStr;
	this->second_str = bStr;
}

int PalindromChecker::checkPalHelper(int i, int j, int k, int m) {
	//std::cout << "i: " << i << " j: " << j << " k: " << k << " m: " << m << std::endl;
	bool i_j_passed 	=  (i > j);
	bool m_k_passed 	=  (k > m);
	bool i_j_overlapped =  (i == j);
	bool m_k_overlapped =  (k == m);

	if ( (i_j_passed || i_j_overlapped) && (m_k_passed || m_k_overlapped)) {
		
		if (i_j_overlapped && m_k_overlapped) {
			return (this->first_str[i] == this->second_str[k]);
		}

		return true;
	} else {
		if ( (!m_k_overlapped && !m_k_passed) && (this->second_str[k] == this->second_str[m])) {
			return checkPalHelper(i, j, k + 1, m - 1);
		}

		else if ((!i_j_passed && !i_j_overlapped) && (this->first_str[i] == this->first_str[j])) {
			return checkPalHelper(i + 1, j - 1, k, m);
		}

		else if ((!i_j_passed && !m_k_passed) && (this->first_str[i] == this->second_str[m])) {
			return checkPalHelper(i + 1, j, k, m - 1);
		}

		else if ((!i_j_passed && !m_k_passed) && (this->second_str[k] == this->first_str[j])) {
			return checkPalHelper(i, j - 1, k + 1, m);
		}
	}

	return false;
}


int PalindromChecker::checkPal(){
	int i = 0;
	int j = first_str.size() - 1;
	int k = 0;
	int m = second_str.size() - 1;
	return PalindromChecker::checkPalHelper(i, j, k, m);
}

vector<PalindromChecker *> inputReader(){
	string line;
	int _numberProblems;
	cin >> _numberProblems;
	PalindromChecker* curPalChecker;
	vector<PalindromChecker *> palVec;

	while (getline(cin, line)) {
		if (!line.empty()) {
			string strA = line;
			getline(cin,line);
			string strB = line;
			curPalChecker = new PalindromChecker(strA, strB);
			palVec.push_back(curPalChecker);
		} 
	}
	return palVec;
}

int main(){
	for (PalindromChecker* palChecker: inputReader()) {
		if (palChecker->checkPal()) cout << "YES" << endl;
		else cout << "NO" << endl;
	}
}
