#ifndef MORE_UNIX_H_
#define MORE_UNIX_H_

#include <iostream>
#include <vector>
#include <sstream>
#include <iterator>

using namespace std;

class ClassArranger {
public:
	ClassArranger(int weekNum);
	~ClassArranger(){};
	int _weekNum;
	std::vector<int> profB;
	std::vector<int> profC;
	int longestHours();
};

#endif