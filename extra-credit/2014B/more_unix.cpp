/* 2014 No.B */
/*	
	I think tis code is very self explaintory. 
	startB[1] = profB[0];
	startC[1] = profC[0];

	for (int i = 2; i <= _weekNum; i++) {
		startB[i] = max(startB[i-1], startC[i-2]) + profB[i-1];
		startC[i] = max(startC[i-1], startB[i-2]) + profC[i-1];
	}

	int maxB = startB[_weekNum];
	int maxC = startC[_weekNum];
*/
#include "more_unix.hpp"

vector<int> split(string str) {
    std::istringstream is( str );
    int n;
    vector<int> lineVec;
    while( is >> n ) lineVec.push_back(n);
	return lineVec;
}

vector<ClassArranger *> inputReader(){
	string line;
	int _numWeeks;
	cin >> _numWeeks;
	ClassArranger* curWeek;
	vector<ClassArranger *> weekVec;

	while (getline(cin, line)) {
		if (!line.empty()) {
			vector<int> lineVector = split(line);
			if (lineVector.size() == 1) {
				curWeek = new ClassArranger(lineVector[0]);
 				weekVec.push_back(curWeek);
			} 
			else {
				curWeek->profB.push_back(lineVector[0]);
				curWeek->profC.push_back(lineVector[1]);
			}
		} 
	}
	return weekVec;
}

ClassArranger::ClassArranger(int weekNum) { _weekNum = weekNum; }

int ClassArranger::longestHours(){
	int * startB = new int[_weekNum + 1];
	int * startC = new int[_weekNum + 1];

	for (int i = 0; i < _weekNum + 1; i++) {
		startC[i] = 0;
		startB[i] = 0;
	}

	startB[1] = profB[0];
	startC[1] = profC[0];

	for (int i = 2; i <= _weekNum; i++) {
		startB[i] = max(startB[i-1], startC[i-2]) + profB[i-1];
		startC[i] = max(startC[i-1], startB[i-2]) + profC[i-1];
	}

	int maxB = startB[_weekNum];
	int maxC = startC[_weekNum];

	delete[] startB;
	delete[] startC;
	return max(maxB, maxC);
}


int main() {
	vector<ClassArranger*> weekVec = inputReader();
	int counter = 0;
	for (ClassArranger* arranger: weekVec) {
		cout << "Case " << ++counter << ": " << arranger->longestHours() << endl;
	}
}

