#ifndef SOUND_CHECKER_H_
#define SOUND_CHECKER_H_

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <map>
#include <iterator>

using namespace std;

class SoundChecker {
public:
	SoundChecker(string receivedSound);
	~SoundChecker(){};
	string _receivedSound;
	std::vector<string> _options;
	double score(string word, double ** table);
	double possibilityLookUp(char a, char b, double ** table);
	string bestMatch(double **);
};

#endif