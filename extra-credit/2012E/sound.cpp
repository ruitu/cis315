/* 2012 No.E */

/*	
let the heard string be h and target string be t
let D[i][j] be the maxium possibility at t[0:i] and h[0:j]
let soundData be the look up table. Therefore we have:

D[i][j] = max( D[i-1][j-1] + table[i][j],  D[i, j-1] )
*/

#include "sound.hpp"
SoundChecker::SoundChecker(string receivedSound) { _receivedSound = receivedSound;}

vector<string> split(string str) {
    std::istringstream is( str );
    string n;
    vector<string> lineVec;
    while( is >> n ) lineVec.push_back(n);
	return lineVec;
}

vector<SoundChecker *> inputReader(){
	string line;
	SoundChecker* curInput;
	vector<SoundChecker *> soundCheckerVec;
	int numProblem;
	cin >> numProblem;

	while (getline(cin, line)) {
		if ( !line.empty() ) {
			vector<string> lineVector = split(line);
			curInput = new SoundChecker(lineVector[0]);
			soundCheckerVec.push_back( curInput );
			for (auto it = next(lineVector.begin()); it != lineVector.end(); it++ ) {
				curInput->_options.push_back(*it);
			}
		}
	}
	return soundCheckerVec;
}

double SoundChecker::score( string word, double ** table ) {
	size_t recivedSize = _receivedSound.size() + 1;
	size_t wordSize    = word.size() + 1;

	/* allocate memory */
	double ** mem = new double*[wordSize];
	for (int i = 0; i < wordSize; i++) { mem[i] = new double[recivedSize + 1]; }
		
	/* initalizing memory */
	for (int i = 0; i < wordSize; i++) {
		for (int j = 0; j < recivedSize; j++) {
			mem[i][j] = 0;
		}
	}

	/* computation */
	for (int i = 1; i < wordSize; i++) {
		for (int j = i; j < recivedSize; j++) {
			mem[i][j] = max( mem[i-1][j-1] + possibilityLookUp(word[i-1], _receivedSound[j-1], table), mem[i][j-1]);
		}
	}

	return mem[word.size()][_receivedSound.size()];
}



double SoundChecker::possibilityLookUp(char a, char b, double ** table) {
	string charArray = "pbmfvtdsznSZkgNlLr";
	map<char, int> indexMap;
	for (int i = 0; i < charArray.size(); i++) { indexMap[charArray[i]] = i; }
	return table[indexMap[a]][indexMap[b]];
}



string SoundChecker::bestMatch(double ** table) {
	vector<double> scoreVec;
	for (string word: _options) {
		scoreVec.push_back(score(word, table));
	}

	int maxindex = 0;
	double maxscore = scoreVec[0];

	for (int i = 0; i < scoreVec.size(); i++) {
		if (scoreVec[i] > maxscore) {
			maxscore = scoreVec[i];
			maxindex = i;
		}
	}

	if (maxscore == 0) { return "What?"; }
	return _options[maxindex];
}


void readSoundData( double ** lookupTable ) {
  	ifstream file("sound.txt");
	for (int i = 0; i < 18; i++) {
  		for (int j = 0; j < 18; j++) {
      		file >> lookupTable[i][j];
    	}
  	}
  	file.close();
}



int main() {
	double ** lookupTable = new double * [18];
	for (int i = 0; i < 18; i++) { 
		lookupTable[i] = new double[18]; 
	}

	readSoundData( lookupTable );
	vector<SoundChecker*> soundVec = inputReader();
	for (SoundChecker* checker: soundVec) {
		cout << checker->bestMatch(lookupTable) << endl;
	}
}