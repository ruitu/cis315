#include "graph.hpp"
/* Edge */
Edge::
Edge(int name, int weight){
	_name = name;
	_weight = weight;
}

int Edge::
getName() { return _name; }

int Edge::
getWeight() { return _weight; }



/* node */
Node::
Node(int name) { _name = name; }

vector<Edge*> Node::
getEdgeList() { return _edgeVec; }

int Node::
getName() { return _name; }

void Node::
addEdge(Edge * newEdge) { _edgeVec.push_back( newEdge ); }

/* Graph */
Graph::
Graph(int numNode, int numEdge) {
	_numNode = numNode;
	_numEdge = numEdge;
}

void Graph::
addNode(Node * newNode) { _nodeVec.push_back( newNode ); }

Node* Graph::
getNode(int name) {
	for (Node* node : _nodeVec) {
		if (node->getName() == name) {
			return node;
		}
	}
	return NULL;
}



vector<Node*> Graph::
getNodeList() { return _nodeVec; }

void Graph::
printGraph(){ 
	cout << "graph size: " << _nodeVec.size() << endl;
	for (Node* node : _nodeVec) {
		cout << node->getName() << " -> ";
		for ( Edge* edge: node->getEdgeList() ) {
			cout << "(" << edge->getName() << ", " << edge->getWeight() << ") ";
		}
		cout << endl;
	}
}

vector<int> GraphParser::
split(string str) {
    std::istringstream is( str );
    int n;
    vector<int> lineVec;
    while( is >> n ) lineVec.push_back(n);
	return lineVec;
}


vector<Graph*> GraphParser::
getGraphList() { return _graphVec; }


void GraphParser::
readGraphInput(){
	string line;
	cin >> _numGraph;
	Graph * curGraph;
	Node * curNode;

	
	while (getline(cin, line)) {
		if (!line.empty()) {
			vector<int> lineVector = split(line);
		
			if (lineVector.size() == 2) {
				curGraph = new Graph( lineVector[0], lineVector[1] );
				_graphVec.push_back( curGraph );
			} 

			else {

				curNode  = curGraph->getNode(lineVector[0]);
				Node* edgeNode = curGraph->getNode(lineVector[1]);
				
				if (curNode == NULL) {
					curNode = new Node(lineVector[0]);
					curGraph->addNode(curNode);				
				}

				if (edgeNode == NULL) {
					Node* newNode = new Node(lineVector[1]);
					curGraph->addNode(newNode);
				}

				curNode->addEdge(new Edge(lineVector[1], lineVector[2]));
			} 
		}
	}
}

int Graph::
rightPath() {
	int * _weightSum = new int[_numNode + 1];
	int * _numPath   = new int[_numNode + 1];
	
	_weightSum[1] = 0;
	_numPath[1]   = 1;

	for (size_t j = 2; j <= _numNode; j++) { _numPath[j]   = 0; }
	for (size_t i = 2; i <= _numNode; i++) { _weightSum[i] = 0; }

	for (Node* node: getNodeList()) {
		for (Edge* edge : node->getEdgeList()) {
			_numPath[edge->getName()]   += _numPath[node->getName()];
			_weightSum[edge->getName()] = _weightSum[edge->getName()] + _weightSum[node->getName()] + edge->getWeight();
		}
	}

	int numpath = _numPath[_numNode];
	int pathsum = _weightSum[_numNode];
	int result  = std::round(pathsum / numpath);
	cout << ": " << numpath << " " << result;

	delete [] _weightSum;
	delete [] _numPath;
	return result;
}



int main(){
	GraphParser parser;
	parser.readGraphInput();
	int counter = 0;
	for(Graph* graph: parser.getGraphList()) {
		cout << "graph " << ++counter << ": ";
		cout << graph->rightPath() << endl;
	}


	return 0;
}
