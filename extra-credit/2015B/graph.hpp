#ifndef GRAPH_H_
#define GRAPH_H_

#include <iostream>
#include <vector>
#include <sstream>
#include <iterator>

using namespace std;

class Edge {
public:
	Edge(int name, int _weight);
	~Edge(){};
	int _name;
	int _weight;
	int getName();
	int getWeight();
};


class Node{
public:
	Node(int name);
	~Node();
	int getName();
	vector<Edge*> getEdgeList();
	void addEdge(Edge * newEdge);
private:
	int _name;
	vector<Edge*> _edgeVec;
};


class Graph{
public:
	Graph(int numNode, int numEdge);
	~Graph(){};
	void addNode(Node * newNode);
	void printGraph();
	vector<Node*> getNodeList();
	Node* getNode(int name);
	int rightPath();
private:
	int _numNode;
	int _numEdge;
	vector<Node*> _nodeVec;
	int rightPathHelper( int* weightSum, int* numPath);


};



class GraphParser {
public:
	GraphParser(){};
	~GraphParser(){};
	int _numGraph;
	vector<Graph*> _graphVec;
	void readGraphInput();
	vector<int> split(string str);
	vector<Graph*> getGraphList();
};

#endif