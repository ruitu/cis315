// Rui Tu CIS 315 Programming Assignemnt 1 Winter 2016
#include "../header/Neighbor.hpp"
/********************* <Neighbor> *********************/
Neighbor::Neighbor (int selfName) : _nextNeighbor(0), _selfName(selfName) {
	this->_selfIndex = selfName - 1;
}


Neighbor* Neighbor::getNext(){ return this->_nextNeighbor; }
void Neighbor::setNext(Neighbor * nextNeighbor){ this->_nextNeighbor = nextNeighbor; }
int Neighbor::getIndex(){ return this->_selfIndex; }
int Neighbor::getName(){ return this->_selfName; }
/********************* </ Neighbor> *********************/
