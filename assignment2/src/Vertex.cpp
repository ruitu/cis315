#include "../header/Vertex.hpp"
/********************* < Vertex > *********************/
Vertex::Vertex(int name)
{
	_name 			 = name;
	_index 			 = _name - 1;
	_inNeighborNum   = 0;
	_outNeighborNum  = 0;
	_outNeighborHead = 0;
	_inNeighborHead	 = 0;
}

// getters
int Vertex::getIndex(){ return _index; }
int Vertex::getName(){ return _name; }
Neighbor* Vertex::getInNeighborHead() { return _inNeighborHead; }
Neighbor* Vertex::getOutNeighborHead(){ return _outNeighborHead; }

// setters
void Vertex::addInNeighbor(Neighbor* newInNeighbor)
{
	if (_inNeighborNum == 0) {
		_inNeighborHead = newInNeighbor;
	} else {
		newInNeighbor->setNext(_inNeighborHead);
		_inNeighborHead = newInNeighbor;
	}

	_inNeighborNum++;
}

void Vertex::addOutNeighbor(Neighbor* newOutNeighbor)
{
	if (_outNeighborNum == 0) {
		_outNeighborHead = newOutNeighbor;
	} else {
		newOutNeighbor->setNext(_outNeighborHead);
		_outNeighborHead = newOutNeighbor;
	}
	_outNeighborNum++;
}
/********************* < / Vertex > *********************/
