// Rui Tu CIS 315 Programming Assignemnt 1 Winter 2016
#include "../header/Graph.hpp"
#include "../header/Neighbor.hpp"
#include "../header/Vertex.hpp"

Graph::Graph(bool isDirected, int verticesNum, int edgesNum) : _currentVerticesNum(0) {
	this->_verticesList = new Vertex *[this->_verticesNum = verticesNum];
	this->_isDirected = isDirected;

	// initalize them to nullptr value
	for (int i = 0; i < verticesNum; i++) {
		this->_verticesList[i] = 0;
	}
}


void Graph::addEdge(int vertex, int neighbor) {
	if ((this->_verticesList[ vertex - 1 ]) == 0)
		this->_verticesList[ vertex - 1 ] = new Vertex(vertex);

	if ((this->_verticesList[ neighbor - 1 ]) == 0)
		this->_verticesList[ neighbor - 1 ] = new Vertex( neighbor );

	this->_verticesList[ vertex - 1 ]->addOutNeighbor( new Neighbor(neighbor) );
}


/* return the array used to store all the vertex objects */
Vertex** Graph::getVerticesList(){ return this->_verticesList; }
Vertex*  Graph::getVertexByIndex(int index){ return this->_verticesList[index]; }
Vertex*  Graph::getVertexByName(int Name){ return this->_verticesList[ Name - 1 ]; }
	int  Graph::graphSize(){ return this->_verticesNum; }


// done:
int Graph::longestPathBetween(int beginVertexName, int endVertexName) {
	int* distanceMap = new int[graphSize()];
  	// initialize it to 0;
	for (int i = 0; i < graphSize(); i++)
		distanceMap[i] = -1;

	distanceMap[0] = 0;

	int result = longestPathHelper(beginVertexName, endVertexName, distanceMap);
  	// deallocate array
	delete [] distanceMap;
	return result;
}

//done:
int Graph::longestPathHelper(
	int beginVertexName,
	int endVertexName,
	int* distanceMapPtr)
{
	for (int i = 0; i < graphSize(); i++) {
		Vertex*   curVertex   = getVertexByIndex(i);
		Neighbor* curNeighbor = curVertex->getOutNeighborHead();
		while (curNeighbor != 0) {
			int newDistance = distanceMapPtr[i] + 1;
			if (newDistance > distanceMapPtr[curNeighbor->getIndex()])
				distanceMapPtr[curNeighbor->getIndex()] = newDistance;
			curNeighbor = curNeighbor->getNext();
		}
	}
	return distanceMapPtr[endVertexName - 1];
}



int Graph::shortestPathBetween(int beginVertexName, int endVertexName)
{
	int* distanceMap = new int[graphSize()];
  	// initialize it to 0;
	for (int i = 0; i < graphSize(); i++)
		distanceMap[i] = -1;

	distanceMap[0] = 0;

	int result = shortestPathHelper(beginVertexName, endVertexName, distanceMap);
  	// deallocate array
	delete [] distanceMap;
	return result;
}

int Graph::shortestPathHelper(
	int beginVertexName,
	int endVertexName,
	int* distanceMapPtr)
{
	for (int i = 0; i < graphSize(); i++) {
		Vertex*   curVertex   = getVertexByIndex(i);
		Neighbor* curNeighbor = curVertex->getOutNeighborHead();
		while (curNeighbor != 0) {
			int newDistance = distanceMapPtr[i] + 1;
			if ( distanceMapPtr[curNeighbor->getIndex()] == -1 ||
				newDistance < distanceMapPtr[curNeighbor->getIndex()])
				distanceMapPtr[curNeighbor->getIndex()] =  newDistance;

			curNeighbor = curNeighbor->getNext();
		}
	}
	return distanceMapPtr[endVertexName - 1];
}


int Graph::numOfPathes(int beginVertexName, int endVertexName) {
	int numPath = 0;
	int* numPathPointer = &numPath;
	numOfPathesHelper(beginVertexName, endVertexName, numPathPointer);
	return numPath;
}


int Graph::numOfPathesHelper(int beginVertexName, int endVertexName, int* numPathPtr) {
	int beginIndex = beginVertexName - 1;
	int endIndex   = endVertexName   - 1;

	if (beginIndex == endIndex)
		return 1;

	if (*numPathPtr == 0) {
		Neighbor* curNeighbor= getVertexByIndex(beginIndex)->getOutNeighborHead();
		int sum = 0;
		while (curNeighbor) {
			sum += numOfPathesHelper(curNeighbor->getName(), endVertexName, numPathPtr);
			* numPathPtr = sum;
			curNeighbor = curNeighbor->getNext();
		}
	}
	return * numPathPtr;
}

Vertex* Graph::DFS(int beginVertexName, int endVertexName) {


	return;
}
