// Rui Tu CIS 315 Programming Assignemnt 1 Winter 2016
#include "./header/Graph.hpp"
#include <vector>
using namespace std;

vector<int> split(string str, char delimiter) {
  vector<int> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;

  while(getline(ss, tok, delimiter))
      internal.push_back(stoi(tok));

  return internal;
}

void readAGraph(vector< vector<int> > graph){
    int vertexNum, edgeNum;

    vertexNum = graph[0][0] ;
    edgeNum   = graph[1][0] ;

    int vertex;
    int neighbor;

    Graph list(true, vertexNum, edgeNum);

    for (size_t i = 2; i < graph.size(); i++) {
        list.addEdge(graph[i][0], graph[i][1]);
    }


    while (cin >> vertex >> neighbor)
        list.addEdge(vertex, neighbor);

    int longestPath  = list.longestPathBetween(1, list.graphSize());
    int shortestPath = list.shortestPathBetween(1, list.graphSize());
    int numOfPathes  = list.numOfPathes(1, list.graphSize());

    //std::cout <<  file << std::endl;
    std::cout <<  "Shortest path: " << shortestPath << std::endl;
    std::cout <<  "Longest  path: " << longestPath << std::endl;
    std::cout <<  "Number of pathes: " << numOfPathes << std::endl;
}


int main() {
    string line;
    vector< vector<int> > graphInput;
    vector < vector< vector<int> > > multiGraphs;


    while (getline(cin, line)){
        if (!line.empty())
            graphInput.push_back(split(line, ' '));
    }

    if (graphInput.size() < 4) {
        std::cout << "Are you kidding me?" << std::endl;
        return 0;
    }

    size_t i = 1;
    while (i < graphInput.size()) {
        vector< vector<int> > aGraph;
        aGraph.push_back(graphInput[i]);
        aGraph.push_back(graphInput[i+1]);
        i += 2;

        while (graphInput[i].size() > 1) {
            aGraph.push_back(graphInput[i]);
            i += 1;
        }
        //multiGraphs.push_back(aGraph);
        readAGraph(aGraph);
        std::cout << " " << std::endl;
    }

  return 0;
}
