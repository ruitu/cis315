#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#ifndef HEIGHBOR_H
#define HEIGHBOR_H

class Neighbor {
public:
    Neighbor (int name);
    ~Neighbor();
    bool hasNext();
    void setNext(Neighbor *);
    Neighbor* getNext();
    int getIndex();
    int getName();

private:
  Neighbor *_nextNeighbor;
  int _selfIndex;
  int _selfName;
};

#endif
