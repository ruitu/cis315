// Rui Tu CIS 315 Programming Assignemnt 1 Winter 2016
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "../header/Vertex.hpp"

using namespace std;

#ifndef GRAPH_H
#define GRAPH_H


class Graph{
public:
  Graph (bool isDirected, int verticesNum, int edgesNum);
  ~Graph();
  Vertex** getVerticesList();
  Vertex*  getVertexByIndex(int vertexIndex);
  Vertex*  getVertexByName(int vertexName);
  Vertex*  DFS(int beginVertexName, int endVertexName);

  int graphSize();
  int shortestPathBetween(int beginingVertexName, int endingVertexName);
  int longestPathBetween(int beginingVertexName, int endingVertexName);
  int numOfPathes(int beginVertexName, int endIndexName);
  bool isEdge(int vertexIndexA, int vertexIndexB);
  void addEdge(int vertex, int neighbor);

  //~Graph();
private:
  Vertex** _verticesList;   // Length of a box
  bool _isDirected;
  int _verticesNum;
  int _currentVerticesNum;

  int longestPathHelper(int beginVertex, int endVertex, int* distanceMapArray);
  int shortestPathHelper(int beginVertex, int endVertex, int* distanceMapArray);
  int numOfPathesHelper(int beginVertexName, int endIndexName, int * numPath);
};


#endif
