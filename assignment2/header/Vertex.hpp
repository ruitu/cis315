#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "Neighbor.hpp"

#ifndef VERTEX_H
#define VERTEX_H

class Vertex {
public:
  //constructors
  Vertex(int name);
  ~Vertex();
  // getters
  int getName();
  int getIndex();
  Neighbor* getInNeighborHead();
  Neighbor* getOutNeighborHead();


  // methods
  void addInNeighbor(Neighbor* newInNeighbor);
  void addOutNeighbor(Neighbor* newOutNeighbor);


private:
  int _name;
  int _index;
  int _inNeighborNum;
  int _outNeighborNum;
  Neighbor* _inNeighborHead;
  Neighbor* _outNeighborHead;

};

#endif
