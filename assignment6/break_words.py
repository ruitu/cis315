# consulted Andrew Hill

import fileinput
from math import *
class Parser:
    def __init__(self):
        self._dict       = {}
        self._input_list = []
        self.read_dict()
        self.read_input()

    def read_dict(self):
        temp_list = []
        with open("./inputs/diction10k.txt", "r") as text:
            for line in text:
                temp_list.append(line.rstrip('\n').rstrip('\r'))
        self._dict = set(temp_list)

    def read_input(self):
        counter = 0
        for line in fileinput.input():
            if (counter > 0):
                self._input_list.append(line.rstrip('\n').rstrip('\r'))
            counter += 1;

        self._input_list = list(line for line in self._input_list if line)


    def parse_line_recursive(self, line):
        size = len(line)
        mem = [-1] * (size + 1)
        mem[len(line)] = True
        pos = [-1] * (size + 1)

        if (self.split(0, line, mem, pos)):
            self.print_line(pos, mem, line)
            return True
        else:
            print("CANNOT be splitted")
            return False


    def split(self, i, line, mem, posi):
        size = len(line)
        if (mem[i] != -1):
            return mem[i];
        else:
            for j in range(i, size):
                mem[i] = False or (line[i:j + 1] in self._dict and self.split(j+1, line, mem, posi))
                if (mem[i]):
                    posi[i] = j
                    break;

        return mem[i]

    def parse_line_iterative(self, line):
        size = len(line)
        mem = []
        for i in range(0, len(line) + 1): mem.append(False)
        pos = []
        for i in range(0, len(line) + 1): pos.append(-1)
        mem[size] = True
        for i in range(size,-1,-1):
            for j in range(i,size):
                if(line[i:j+1] in self._dict and mem[ j + 1 ]):
                    mem[i]=True
                    pos[i]=j
        if mem[0]:
            print("Can be splited:")
            self.print_line(pos, mem, line)
        else:
            print("CANNOT be splited")

        return mem

    def print_line(self, pos, mem, line):
        next_index, previous_index, newString = pos[0], 0, ""
        while (next_index < len(line) and next_index != -1):
            newString += line[previous_index : next_index + 1] + " "
            previous_index = next_index + 1
            next_index = pos[next_index + 1]
        print(newString)

    def print_result(self):
        for i in range(len(self._input_list)):
            print("phrase number", i + 1)
            print("Iterative Attempt")
            self.parse_line_iterative(self._input_list[i])
            print()

            print("Recursive Attempt")
            self.parse_line_recursive(self._input_list[i])
            print()


if __name__ == '__main__':
    parser = Parser()
    parser.print_result()
